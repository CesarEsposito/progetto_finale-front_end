import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Ability } from './ability'
import { MessageService } from './message.service';
import { Hero } from './hero';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AbilityService {

  // URL to web api
  private abilitiesUrl = 'http://localhost:8080/Hero';
  ability: Ability;

  constructor(private http: HttpClient,
    private messageService: MessageService) {
    messageService.add('Creazione di AbilityService');
  }



  getAll(): Observable<Ability[]> {
    return this.http.get<Ability[]>(this.abilitiesUrl
      .concat("/abilities"));
  }

  getAlladdableAbilities(idHero: number): Observable<Ability[]> {
    return this.http.get<Ability[]>(this.abilitiesUrl
      .concat("/addableAbilities?idHero=" + idHero));
  }

  getHeroAbilities(idHero: number): Observable<Ability[]> {
    return this.http.get<Ability[]>(this.abilitiesUrl
      .concat("/listAbility?idHero=" + idHero));
  }

  addAbility(idHero: number, idAbility: number) {

    return this.http.post<Ability[]>(this.abilitiesUrl
      .concat("/addAbilities?idHero=" + idHero + "&idAbility=" + idAbility)
      , httpOptions);
  }
  delAbility(idHero, idAbility) {
    return this.http.delete<Ability[]>(this.abilitiesUrl
      .concat("/delAbilities?idHero=" + idHero + "&idAbility=" + idAbility)
      , httpOptions);
  }



  addNewAbility(idHero: number, ability: string, description: string): Observable<Ability> {
    console.log(description);
    if (ability == null) { ability = "" }

    if (description == null) { description = "" }

    return this.http.post<Ability>(this.abilitiesUrl.
      concat("/addNewAbility?idHero=" + idHero + "&ability=" + ability + "&description=" + description),
      ability, httpOptions)
      ;


  }

  modifyAbility(idAbility: number, ability: string, description: string) {

    if (ability == null) { ability = "" }
    if (description == null) { description = "" }
    


    return this.http.put<Ability>(this.abilitiesUrl.
      concat("/modifyAbility?idAbility=" + idAbility +
        "&ability=" + ability + "&description=" + description), ability, httpOptions
    )
      ;
  }


}