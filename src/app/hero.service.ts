import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Hero } from './hero';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { UrlTree } from '@angular/router';
import { stringify } from '@angular/core/src/render3/util';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  // URL per le web api
  private heroesUrl = 'http://localhost:8080/Hero';

  constructor(private http: HttpClient,
    private messageService: MessageService) {
    messageService.add('Creazione di HeroService');
  }

  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.heroesUrl.concat('/all'))
      .pipe(
        tap(heroes => this.log(`fetched ${heroes.length} heroes`)),
        catchError(this.handleError('getHeroes', []))
      );
  }

  // public Observable<Hero> getHero(int id)
  /** GET hero by id. Lanciera' Err. 404 se non l'id */
  getHero(id: number): Observable<Hero> {
    const url = `${this.heroesUrl.concat("/detail?id=")}${id}`;
    return this.http.get<Hero>(url).pipe(
      tap(_ => this.log(`fetched hero id=${id}`)),
      catchError(this.handleError<Hero>(`getHero id=${id}`))
    );
  }

  /** POST: aggiunge un nuovo Eroe al server */
  addHero(hero: Hero, ): Observable<Hero> {
    console.log(hero.email);
    if (hero.email == null) { hero.email = "" }
    return this.http.post<Hero>(this.heroesUrl.
      concat("/add?name=" + hero.name + "&email=" + hero.email),
      hero, httpOptions)
      .pipe(
        tap((h: Hero) => this.log(`added hero w/ id=${h.id}`)),
        catchError(this.handleError<Hero>('addHero'))

      );


  }

  /** PUT: Aggiorna l'Eroe sul server */
  updateHero(hero: Hero): Observable<any> {
    return this.http.put(this.heroesUrl.concat("/update"), hero, httpOptions).pipe(
      tap(_ => this.log(`updated hero id=${hero.id}`)),
      catchError(this.handleError<any>('updateHero'))
    );

  }

  /** DELETE: Elimina l'Eroe dal server*/

  // overload in TypeScript, l'argomento hero puo' essere di tipo
  // number oppure Hero. In generale posso usare la sintassi
  // (nomeVar: tipo1 | tipo2 | tipo3 ...)
  deleteHero(hero: Hero | number): Observable<Hero> {
    const id = typeof hero === 'number' ? hero : hero.id;
    const url = `${this.heroesUrl.concat("/delete?id=")}${id}`;

    return this.http.delete<Hero>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted hero id=${id}`)),
      catchError(this.handleError<Hero>('deleteHero'))
    );
  }

  /* GET degli Eroi il cui nome contiene i caratteri della variabile term */
  searchHeroes(term: string): Observable<Hero[]> {
    if (!term.trim()) {
      //se non trova nulla, ritorna un array vuoto di tipo hero.
      return of([]);
    }
    this.log(`Searching for "${term}"`);
    // api/heroes/?name=superman
    return this.http.get<Hero[]>(`${this.heroesUrl.concat("/search")}?name=${term}`).pipe(
      tap(heroes => this.log(`found ${heroes.length} heroes matching "${term}"`)),
      catchError(this.handleError<Hero[]>('searchHeroes', []))
    );
  }




  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }


  changeOrAddHeroImage(idHero: number, url: string) {
    return this.http.post<Hero[]>(this.heroesUrl
      .concat("/addOrChangeImg?idHero=" + idHero + "&url=" + url)
      , httpOptions);

  }

  getWallPaper(call: number): Observable<string> {
    return this.http.get<string>(this.heroesUrl.concat('/getWall?call=' + call))
  }


}
