import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {
  heroes: Hero[];
  hero: Hero;
  wallpaper: string;
  call = 0
  heroesNumber: number;
  heroesNumberString: string;
  constructor
    (private heroService: HeroService,
    private messageService: MessageService,
  ) {
    messageService.add('Creazione di HeroesComponent');

  }

  ngOnInit() {
    this.getHeroes();
    this.getWallPaper(this.call);
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe((heroes) => {
        this.heroes = heroes;
        this.heroesNumber = this.heroes.length
        this.herstringnumbercases()
      });

  }

  add(name: string, email: string): void {
    name = name.trim();
    email = email.trim();

    if (name) {
      this.heroService.addHero({ name, email } as Hero)
        .subscribe(hero => this.getHeroes());
    }
  }

  delete(hero: Hero): void {
    this.heroService.deleteHero(hero)
      .subscribe(() => {
        this.heroes = this.heroes.filter(h => h !== hero)
        this.getHeroes();
      });
  }

  sortNameAsc(): void {
    this.heroes.sort((a, b) =>
      (a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase())))

  }

  sortNameDesc(): void {
    this.heroes.sort((a, b) =>
      (b.name.toLocaleLowerCase().localeCompare(a.name.toLocaleLowerCase())))
  }

  sortIdMajor(): void {
    this.heroes.sort((a, b) => {
      if (a.id > b.id) return 1;
    }
    )
  }

  sortIdMinor(): void {
    this.heroes.sort((a, b) => {
      if (a.id < b.id) return 1;
    }
    )
  }


  getWallPaper(call: number): void {
    this.heroService.getWallPaper(call)
      .subscribe((wallpaper) => this.wallpaper = wallpaper);
  }


  herstringnumbercases() {
    switch (this.heroesNumber) {

      case 0:
        this.heroesNumberString = "Non sono presenti Eroi nella lista"
        break;
      case 1:
        this.heroesNumberString = "E' presente "+this.heroesNumber+" solo Eroe nella lista"
        break;
      default:
        this.heroesNumberString = "Sono presenti "+this.heroesNumber+" Eroi nella lista"
        break;
    }

  }

}
