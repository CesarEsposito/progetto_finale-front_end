import { Component, Input, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Ability } from '../ability'
import { AbilityService } from '../ability.service'

@Component({
    selector: 'app-hero-detail',
    templateUrl: './hero-detail.component.html',
    styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
    hero: Hero;
    ability: Ability;
    abilities: Ability[];
    sel = 1;
    constructor(
        private route: ActivatedRoute,
        private abilityService: AbilityService,
        private heroService: HeroService,
        private location: Location,

    ) { }

    ngOnInit() {
        this.getHero();
    }

    getHero(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.heroService.getHero(id)
            .subscribe(hero => this.hero = hero);
        this.getAddableAllabilities();

    }

    goBack(): void {
        this.location.back();
    }

    save(): void {
        this.heroService.updateHero(this.hero)
            .subscribe(() => this.goBack());
    }

    previewPortrait(url: string) {
        if (url == null || url == "" || url == undefined) { }
        else
            this.hero.portrait = url;

    }

    changeOrAddHeroImage(idHero: number, url: string) {
        event.stopPropagation();
        this.heroService.changeOrAddHeroImage(idHero, url)
            .subscribe(data => this.ngOnInit());

    }

    getAllabilities(): void {
        this.abilityService.getAll()
            .subscribe(abilities => this.abilities = abilities);
    }

    getAddableAllabilities() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.abilityService.getAlladdableAbilities(id)
            .subscribe(abilities => this.abilities = abilities);
    }

    addAbilityToHero(idHero: number, idAbility: number): void {
        this.abilityService.addAbility(idHero, idAbility)
            .subscribe(data => this.ngOnInit());
    }

    metodofarlocco(): void {

        this.abilities = this.abilities.filter(x => this.hero.listAbility)
        console.log(this.hero.listAbility);
        console.log(this.abilities);

    }
    getHeroabilities(idHero: number) {
        this.abilityService.getHeroAbilities(idHero)
            .subscribe(listAbility => this.hero.listAbility = listAbility);
        this.getAddableAllabilities();

    }
    delAbilityFromHero(idHero: number, idAbility: number): void {
        this.abilityService.delAbility(idHero, idAbility)
            .subscribe(data => this.ngOnInit());
    }



    addAbility(idHero: number, ability: string, description: string): void {
        ability = ability.trim();
        description = description.trim();
        this.abilityService.addNewAbility(idHero, ability, description)
            .subscribe(data => this.ngOnInit());

    }

    modifyAbility(idAbility: number,
        ability: string, description: string) {
        ability = ability.trim();
        description = description.trim();
        this.abilityService.modifyAbility(idAbility, ability, description)
            .subscribe(data => this.ngOnInit());
        this.ngOnInit();
    }

    setBoxDimensions(boxtAbility: string, boxDescription: string, ) {

        var boxtAbilityLength = boxtAbility.length
        var biggestNum = 0;
        console.log(biggestNum);
        console.log(boxtAbility.length);
        if (biggestNum < boxtAbilityLength) { biggestNum = boxtAbilityLength }
        for (var i = 0; i < this.abilities.length; i++) {
            if (boxtAbilityLength[i] > biggestNum) {
                biggestNum = boxtAbilityLength[i];
            }
        }
        console.log(biggestNum);
        return biggestNum*8;
    }

}
