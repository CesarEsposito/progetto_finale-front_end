import { Hero } from "./hero";

export class Ability {
  // nome proprieta': tipo;
  id: number;
  ability: string;
  description: string;
}
